The performance of various complex systems such as  traffic control \cite{tsc}, 
unmanned aerial vehicle (UAV) control \cite{uav}, remote sensing \cite{remotesensing}, 
communication in satellites \cite{satellite} and airlines \cite{airlines} 
depends on a set of tunable parameters (denoted by $\theta$). 
Parameter tuning in such cases is difficult because of bottlenecks 
namely the \emph{black-box} nature of the problem and the 
\emph{curse-of-dimensionality} i.e., the complexity of the search space. In this section, we 
discuss the general theme behind the methods that tackle these bottlenecks and 
their relevance to the problem of tuning the Hadoop parameters.
\subsection{Bottlenecks in Parameter Tuning}
\input{bbopti}
In many complex systems, the exact nature of the dependence of the performance 
on the parameters is not known explicitly i.e., the performance cannot be 
expressed as an analytical function of the parameters. As a result, the 
parameter setting that offers the best performance cannot be computed apriori. 
However, the performance of the system can be observed for any given parameter 
setting either from the system or a simulator of the system. In such a scenario, 
one can resort to \emph{black-box}/\emph{simulation-based} optimization methods 
that tune the parameters based on the output observed from the system/simulator 
without knowing its internal functioning. Figure \ref{bbopti} is a schematic to 
illustrate the black-box optimization procedure. Here, the black-box 
optimization scheme sets the current value of the parameter based on the past 
observations. The way in which past observation is used to compute the current 
parameter setting varies across methods.

An important issue in the context of black-box optimization is the number of 
observations and the cost of obtaining an observation from the system/simulator. 
The term \emph{curse-of-dimensionality} denotes the exponential increase in the 
size of the search space as the number of dimensions increases. 
In addition, in many applications, the parameter $\theta$ belongs to a subset 
$X$ of $\R^n$ (for some positive integer $n>0$). Since it is computationally 
expensive to search such a large and complex parameter space, it is important for 
black-box optimization methods to make as fewer observations as possible.

Hadoop MapReduce  exhibits the above described black box kind of behavior because it is not well 
structured like SQL.
In addition, cross-parameter interactions also affect the performance, and hence 
it is not possible to treat the parameters independent of each other. Besides, the 
problem is also afflicted by the curse-of-dimesionality.
% %Thus, expertise is required to find the better-response parameter setting i.e., 
% the one that outperforms the default setting. In his position paper 
% \cite{Babu10towardsautomatic}, the author made a case for automated tuning of 
% parameters. 
% %A variety of heuristic search methods such as simulated annealing \cite{}, 
% recursive random \cite{} search are useful in searching complex spaces. Whilst 
% these methods yield results in practice they do not handle the 
% curse-of-dimensionality and require careful reduction of the search space 
% \cite{}. Further, such methods do not directly take into account the 
% cross-parameter interactions in the underlying problem.
% \vs
\subsection{Noisy Gradient based optimization}
In order to take the cross-parameter interactions into account, one has to make 
use of the sensitivity of the performance measure with respect to each of the 
parameters at a given parameter setting. This sensitivity is formally known as 
the \emph{gradient} of the performance measure at a given setting. It is 
important to note that it takes only $O(n)$ observations to compute the gradient 
of a function at a given point. However, even $O(n)$ computations are not 
desirable if each observation is itself costly.


% In this section, we provide an intuitive 
% explanation of noisy gradient schemes and defer a more detailed discussion to 
% Section \ref{spsa}. 
Consider the noisy gradient scheme given in \eqref{ngrad} 
below.
\begin{align}\label{ngrad}
\theta_{n+1}&=\theta_n-\alpha_n\big(\nabla f_n +M_n\big),
\end{align}
where $n=1,2\ldots$ denotes the iteration number, $\nabla f_n\in \R^n$ is the 
gradient of function $f$, $M_n\in \R^n$ is a zero-mean noise sequence and $\alpha_n$ is the 
step-size. 
\input{noisegrad}
Fig. \ref{noisegrad} presents an intuitive picture of how a noisy gradient algorithm works. 
Here, the algorithm starts at $\theta_0$ and needs to move to $\theta^*$ which is the 
desired solution. The green lines denote the true gradient step (i.e., $\alpha_n \nabla f_n$) 
and the dotted circles show the region of uncertainty due to the noise term $\alpha_n M_n$. 
The red line denotes the fact that the true gradient is disturbed and the iterates are 
\emph{pushed} to a different point within the region of uncertainty. The idea 
here is to use diminishing step-sizes to filter the noise and eventually move 
towards $\theta^*$. The simultaneous perturbation stochastic approximation (SPSA) 
algorithm is a \emph{noisy gradient} algorithm which works as illustrated in Figure \ref{noisegrad}.
It requires only $2$ observations per iteration. 
We adapt it to tune the parameters of Hadoop. By adaptively tuning the Hadoop parameters,
we intend to optimize the Hadoop job execution time, which is the performance metric (i.e., $f(\theta)$) in 
our experiments. Note that we can also have other performance metrics - like number of records spilled to disk,
Memory and heap usage or number of failed jobs. 
Next, we provide a detailed description of SPSA.

\subsection{Simultaneous Perturbation \\Stochastic Approximation (SPSA)}
\label{spsa}
\input{spsa}