We use Hadoop versions 1.0.3 and 2.7 in our experiments. The SPSA
algorithm described in Sections \ref{autotune},\ref{setup} is implemented
as a process which executes in the Resource Manager (and/or NameNode).
First we justify the selection of parameters to be tuned in our experiments, following which
we give details about the implementation.

\subsection{Parameter Selection}

As discussed in Section \ref{hadoop}, based on the data flow analysis of Hadoop MapReduce
and the Hadoop manual \cite{url_4}, we identify 11 parameters which are found to critically
affect the operation of HDFS and the Map/Reduce operations. 
The list of important parameters that emerged by analyzing the MapReduce implementation are 
listed in Table \ref{tab:opt_parameter}.
Numerous parameters of Hadoop deal with book keeping related tasks, whereas some other parameters
are connected with the performance of underlying operating system tasks.
For e.g., \textit{mapred.child.java.opts} is a parameter related to the Java virtual machine (JVM) of 
Hadoop. We avoid tuning such parameters, which are best left for low-level OS optimization.
Instead we tune parameters which are directly Hadoop dependent, for e.g., number of reducers, I/O utilization
parameter etc. 
However, even with $11$ parameters, the search space is still large and complex. 
To see this, if each parameter can assume say $10$ different values then the search space 
contains $10^{11}$ possible parameter settings. 
Some parameters have been left out because 
either they are  backward incompatible or they incur additional overhead in implementation. 

\subsection{Cluster Setup}
Our Hadoop cluster consists of $25$ nodes. Each node has a 8 core Intel Xeon E3, 2.50 GHz 
processor, $3.5$ TB HDD, $16$ GB memory, 1 MB L2 Cache, 8MB L3 Cache. 
One node works as the 
NameNode and the rest of the nodes are used as DataNodes.
For optimization and evaluation purpose we set the number of map slots to $3$ and 
reduce slots to $2$ per node. Hence, in a single wave of Map jobs processing, the cluster can process 
$24 \times 3 = 72$ map tasks  and $24 \times 2 = 48$ reduce tasks (for more details see \cite{hadoop}). 
HDFS block replication was set to $2$. We use a dedicated Hadoop cluster in our experiments, which
is not shared with any other application.

\subsection{Benchmark Applications}
In order to evaluate the performance of tuning algorithm, we adapt representative MapReduce applications.
The applications we use are listed in Table \ref{tab:opt_parameter}.
Terasort application takes as input a text data file and sorts it. It has three components - 
\emph{TeraGen} - which generates the input data for sorting algorithm, \emph{TeraSort} - algorithm that 
implements sorting and \emph{TeraValidate} - validates the sorted output data. 
The Grep application searches for a particular pattern in a given input file.
The Word Cooccurrence application counts the number of occurrences of a particular word
in an input file (can be any text format).
Bigram application counts all unique sets of two consecutive words in a set of documents, 
while the Inverted index application generates word to document indexing from a list of documents. 
Word Co-occurrence is a popular Natural Language Processing program which
computes the word co-occurrence matrix of a large text collection.
As can be inferred, Grep and Bigram applications are CPU intensive, while
the Inverted Index and TeraSort applications are both CPU and memory intensive.
These benchmark applications can be further categorized as map-intensive, reduce-intensive etc.

\subsection{SPSA Iterations}
SPSA is an iterative algorithm and it runs a Hadoop job with different configurations. 
We refer to these iterations as the \emph{optimization} or the \emph{learning} phase.
The algorithm eventually converges to an optimal value of the configuration parameters. 
The performance metric (the job execution time) corresponding to the converged parameter vector is optimal 
for the corresponding application.
During our evaluations we have seen that SPSA 
converges within 20 - 30 iterations and within each iteration 
it makes two observations, i.e. it executes Hadoop job twice taking the total count of 
Hadoop runs during the optimization phase to 40 - 60. It is of utmost importance 
that the optimization phase is fast otherwise it can overshadow the benefits which 
it provides.


In order to ensure that the optimization phase is fast, we execute the Hadoop jobs 
on a subset of the workload. This is done, since SPSA takes considerable time when
executed on large workloads.
Deciding the size of this ``partial workload'' is very important
as the run time on a small work load will be eclipsed by the job setup and cleanup 
time.  We then consider the technique involved in processing done by Hadoop system 
to find a suitable workload size. Hadoop splits the input data based on the 
block size of HDFS. It then spawns a map for each of the splits and processes each of 
the maps in parallel. The number of the map tasks that can run at a given time is 
upper bounded by the total map slots available in the cluster. Using this fact, the 
size of the partial data set which we use is equal to twice the number of 
map slots in the cluster multiplied by the data block size. Hadoop finishes the 
map task in two waves of the maps jobs which allows the SPSA to capture the 
statistics with a single wave and the cross relations between two successive waves.


Our claim is that the value of configuration parameters which optimize these two 
waves of Hadoop job also optimize all the subsequent waves as those are repetitions of 
similar map jobs. However, the number of reducers to run is completely optimized by SPSA, albeit
for a partial workload size. For the larger (actual) workload, the number of 
reducers decided is based on the ratio of partial work load size to the actual size of workload.
An advantage with SPSA iterations is that these can be halted at any parameter configuration
(for e.g., need for executing a production job on the cluster) and later resumed at the same parameter
configuration where the iterations were halted.

\subsection{Optimization Settings}
For evaluating performance of SPSA on different benchmarks, two waves of map tasks during job execution were ensured.
Further, we selected workloads such that the execution time with default configuration is at least $10$ minutes.
This was done to avoid the scenario where the job setup and cleanup time overshadows 
the actual running time and there is practically nothing for SPSA to optimize.

In the cases of Bigram and Inverted Index benchmark executions, we observed that even with small 
amount of data, the job completion time is high (since they are reduce-intensive operations).
So, due to this reason, we used small sized input data files. Using small sized input data files
resulted in the absence of two waves of map tasks. However, since in these applications,
reduce operations take precedence, the absence of two waves of map tasks did not create
much of a hurdle.

We optimize Terasort using a partial data set of size \textit{30GB}, Grep on \textit{22GB},
Word Co-occurrence on \textit{85GB}, Inverted Index on \textit{1GB} and Bigram count on \textit{200MB}
of data set. In optimization (or learning) phases, for each benchmark, we use the default configuration as the initial point
for the optimization. Table \ref{tab:opt_parameter} indicates
the default values and the values provided by SPSA for the parameters we tune.
For greater sizes of data, we used Wikipedia dataset\cite{puma}($\approx100$GB) for Word co-occurrence, Grep and
Bigram benchmarks


The SPSA algorithm is terminated when either the change in gradient estimate is negligible or 
the maximum number of iterations have been reached. An important point to note is that Hadoop parameters can take
values only in a fixed range. We take care of this by projecting the tuned parameter values 
into the range set (component-wise). A penalty-function can also be used instead. 
If noise level in the function evaluation is high then, 
 it is useful to average several SP gradient estimates (each with 
independent values of $\Delta_{k}$) at a given  $\theta_{k}$.  Theoretical 
justification for net improvements to efficiency by such gradient averaging is 
given in ~\cite{Spall92multivariatestochastic}. We can also use a one evaluation variant of SPSA,
which can reduce the per iteration cost of SPSA. 
But it has been shown  that standard two function  measurement form, 
which we use in our work is more efficient (in terms of total number of loss 
function measurements) to obtain a given level of accuracy in the $\theta$ 
iterate. 

\subsection{Comparison with Related Work}
We compare our method with prior works in the literature on Hadoop performance tuning.
Specifically, we look at Starfish\cite{starfish} as well as Profiling and Performance Analysis-based System 
(PPABS) \cite{ppabs} frameworks.
We briefly describe these methods in Section \ref{relatedwork}. Starfish is designed for Hadoop version 1
only, whereas PPABS works with the recent versions also. Hence, in our experiments we use both versions
of Hadoop. To run Starfish, we use the executable hosted by the authors of \cite{starfish} to profile
the jobs run on partial workloads. Then execution time of new jobs is obtained by running the jobs 
using parameters provided by Starfish. For testing PPABS, we collect datasets as described in \cite{ppabs},
cluster them and find optimized parameters (using simulated annealing) for each cluster.
Each new job is then assigned to one cluster and executed with the parameters optimized for that cluster.

\subsection{Discussion of Results}
\input{opt_parameter}
\input{benchConverge}
\input{benchConverge2}
\input{benchPerfComparev1}
\input{benchPerfComparev2}
Our method starts optimizing with the default configuration,
hence the first entry in Fig. \ref{fig:spsaConv} show the execution time
of Hadoop jobs for the default parameter setting. 
It is important to note that the jumps in the plots are due to the
noisy nature of the gradient estimate and they eventually
die down after sufficiently large number of iterations.
As can be observed from Fig. \ref{fig:spsaConv}, SPSA reduces the 
execution time of Terasort benchmark by $60-63\%$ when compared to default settings
and by $40-60\%$ when compared to Starfish optimizer.
For Inverted Index benchmark the reduction is $~80\%$ when compared to default settings.
In the case of word co-occurrence, the observed reduction is $22\%$ when compared to default settings
and $2\%$ when compared to Starfish.

SPSA finds the optimal configuration while keeping the relation among the parameters in mind. 
For Terasort, a small value $(0.14)$ of \emph{io.sort.spill.percent} will generate a lot of spilled 
files of small size. Because of this, the value of \emph{io.sort.factor} has been increased to $475$ 
from the default value of $10$. This ensures the combination of number of spilled files 
to generate the partitioned and sorted files.
A small value of \emph{shuffle.input.buffer.percent} $(0.14)$ and a large value of 
\emph{inmem.merge.threshold} $(9513)$ may be confusing as both of them act as a threshold 
beyond which in-memory merge of files (output by map) is triggered.
However, map outputs a total bytes of $100$ GB and a total of $2,000,000,000$ files are spilled 
to disk which, effectively make each spilled file of size $50$ bytes. 
Thus filling $0.14\%$ of the memory allocated to Reduce makes $50$ bytes files of which there will be
$9513$. Default value of number of reducers (i.e., $1$) generally does not work in practical situations. 
However, increasing it to a very high number also creates an issue as it results in more network and disk overhead.
As can be observed in Table \ref{tab:opt_parameter},
mapred.compress.map.output is set to true for Terasort benchmark.
This is because, the output data of Map phase has same size as the input data (which might be huge).
Thus, in such scenarios, it is beneficial if the Map output is compressed.
Grep benchmark, on the other hand produces very little map output, and even smaller sized data to be shuffled. 
Hence \emph{io.sort.mb} value is reduced to $50$ from default $100$ (see Table \ref{tab:opt_parameter})
and number of reducers is set to $1$. Further, value of 
\emph{inmem.merge.threshold} has been reduced to $681$ from $1000$ as there is not much data to work on.

Bigram and Inverted Index are computationally expensive operations.
Hence \emph{io.sort.mb} is increased to $751$ and $617$ respectively. 
Both of these applications also generate a reasonable size of data during the map phase,
which implies a lot of spilled files are generated. 
Thus, \emph{inmem.merge.threshold} has been increased to $4201$ and $3542$ respectively.
% How benchmark performs with default, starfish and spsa


% \input{benchPerfComparev2}

\subsection{Advantages of SPSA}
\input{reltable}
The above discussion indicates that SPSA performs well in optimizing Hadoop
parameters. We highlight other advantages (also see Table \ref{tab:rel}) of using 
our proposed method:
\begin{enumerate}
 \item Most of the profiling-based methods (Starfish, MROnline etc), 
 use the internal Hadoop(source code) structure to place ``markers'' for precisely profiling a job. 
 Starfish observes the time spent in each function by using btrace. 
 Small change in the source code make this unusable 
 (clearly Starfish only support Hadoop versions $< 1.0.3$).
 SPSA does not rely on the internal structure of hadoop and only observes the final execution time of the 
 job which can be accessed easily.
 
 \item \textbf{Independent of Hadoop version:} As mentioned previously, profiling-based methods
 are highly dependent on the MapReduce version and any changes in the source code of Hadoop will require
 a version upgrade of these methods. In contrast, our SPSA-based method does not rely on any specific 
 Hadoop version.
 
 \item \textbf{Pause and resume:} SPSA optimizes the parameters iteratively. It starts at a given point (default setting in our case) 
 and then progressively finds a better configuration (by estimating gradient). 
 Such a process can be paused at any iteration and then resumed using the same parameter configuration, where the iteration was stopped.
 This is unlike the profiling-based methods, which need to profile jobs in one go. 
  
 \item SPSA takes into consideration multiple values of execution time of a job for the same parameter setting
 (randomness in execution time).
 This is not the case in other methods, which profile a job only once. 
 Multiple observations helps SPSA to remove the randomness in the job which arise due to the underlying hardware.
 
 \item Parameters can be easily added and removed from the set of tunable parameters, 
 which make our method suitable for scenarios where the user wants to have control over the parameters to be tuned.

 \item \textbf{Profiling overhead:} Profiling of takes a long time (since job run time is not yet optimized 
 during profiling) which adds an extra overhead for these methods like Starfish, PPABS, MROnline etc. For e.g.,
 in our experiments, Starfish profiling executed for $4$ hours, $38$ minutes ($=16680$ seconds) in the case of Word co-occurrence
 benchmark on Wikipedia data of size $4$ GB. Also, Starfish profiled Terasort on $100$ GB of synthetic data for $>2$ hours.
 In contrast, our method does not incur additional ``profiling'' time.
 The SPSA \emph{optimization} time is justified, since each iteration results in learning a better parameter configuration.
\end{enumerate}

% The above highlights are also captured in Table \ref{tab:rel}.