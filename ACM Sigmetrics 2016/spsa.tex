We use the following notation:
\begin{enumerate}
\item $\theta\in X \subset \R^n$ denotes the tunable parameter. Here $n$ is the 
dimension of the parameter space. Also, $X$ is assumed to be a 
compact and convex subset of $\R^n$.
\item Let $x\in \R^n$ be any vector then $x(i)$ denotes its $i^{th}$ 
co-ordinate, i.e., $x=(x(1),\ldots,x(n))$.
\item $f(\theta)$ denotes the performance of the system for parameter $\theta$. 
Let $f$ be a smooth and differentiable function of $\theta$.
\item $\nabla f(\theta)=(\frac{\partial 
f}{\partial\theta(1)},\ldots,\frac{\partial f}{\partial\theta(n)})$ is the 
gradient of the function, and $\frac{\partial f}{\partial \theta(i)}$ is the 
partial derivative of $f$ with respect to $\theta(i)$.
\item $e_i\in \R^n$ is the standard $n$-dimensional unit vector with $1$ in the $i^{th}$ 
co-ordinate and $0$ elsewhere.
\end{enumerate}
Formally the gradient is given by
\begin{align}\label{partial}
\frac{\partial f}{\partial\theta(i)}=\lim_{h\rightarrow 0}\frac{f(\theta+h 
e_i)-f(\theta)}{h}.
\end{align}
In \eqref{partial}, the $i^{th}$ partial derivative is obtained by perturbing 
the $i^{th}$ co-ordinate of the parameter alone and keeping rest of the 
co-ordinates the same. Thus, the number of operations required to compute the 
gradient once via perturbations is of $n+1$. This can be a shortcoming in cases when 
it is costly (i.e., computationally expensive) to obtain measurements of $f$ and 
the number of parameters is large.

The SPSA algorithm \cite{spall} computes the gradient of a function with only 
$2$ or fewer perturbations. Thus the SPSA algorithm is extremely useful in cases 
when the dimensionality is high and the observations are costly. The idea behind 
the SPSA algorithm is to perturb not just one co-ordinate at a time but all the 
co-ordinates together simultaneously in a random fashion. However, one has to 
carefully choose these random perturbations so as to be able to compute the 
gradient. Formally, a random perturbation $\Delta\in \R^n$ should satisfy the 
following assumption.
\begin{assumption}\label{indep}
For any $i\ne j, i=1,\ldots,n, j=1,\ldots,n$, the random variables $\Delta(i)$ 
and $\Delta(j)$ are zero-mean,independent, and the random variable $Z_{ij}$ given by 
$Z_{ij}=\frac{\Delta(i)}{\Delta(j)}$ is such that $\mathbf{E}[Z_{ij}]=0$ and it 
has finite second moment.
\end{assumption}

We now provide an example of random perturbations that satisfies the 
Assumption~\ref{indep}.
\begin{example}
$\Delta\in \R^n$ is such that, each of its co-ordinates $\Delta(i)$s are 
independent Bernoulli random variables taking values $-1$ or $+1$ with equal 
probability, i.e., $Pr\{\Delta(i)=1\}=Pr\{\Delta(i)=-1\}=\frac{1}{2}$ for all 
$i=1,\ldots,n$.
\end{example}
\subsection{Noisy Gradient Recovery from Random Perturbations}
Let $\hat{\nabla} f_\theta$ denote the gradient estimate, and let $\Delta\in 
\R^n$ be any perturbation vector satisfying Assumption~\ref{indep}. Then for any 
small positive constant $\delta >0$, the one-sided SPSA algorithm \cite{chen96,chen97} obtains an estimate of 
the gradient according to equation \eqref{gradest} given below.
\begin{align}\label{gradest}
\hat{\nabla} f_\theta(i)=\frac{f(\theta+\delta 
\Delta)-f(\theta)}{\delta\Delta(i)}.
\end{align}
We now look at the \emph{expected} value of $\hat{\nabla} f_{\theta}(i)$, i.e., 
\begin{align}\label{gradestexp}
\mathbf{E}[\hat{\nabla} f_\theta(i) | \theta]&= \mathbf{E}\left[\frac{f(\theta)+\delta\Delta^\top\nabla f(\theta)+o(\delta^2)-f(\theta)}{\delta \Delta(i)} \;| \theta \right]\nn\\
				    &=\frac{\partial f}{\partial \theta(i)}+\mathbf{E} \left[\sum_{j=1,j\neq i}^n  \frac{\partial f}{\partial \theta(j)}\frac{\Delta(j)}{\Delta(i)} | \theta \right]+o(\delta)\nn\\
				    &=\frac{\partial f}{\partial \theta(i)}+o(\delta).
\end{align}
The third equation follows from the second by noting that 
$\mathbf{E}\left[\frac{\partial f}{\partial \theta(j)}\frac{\Delta(j)}{\Delta(i)} | \theta \right]=0$, 
a fact that follows from the property 
of $\Delta$ in Assumption~\ref{indep}. Thus $\mathbf{E}[\hat{\nabla} 
f_\theta(i)] \ra \nabla f_\theta(i)$ as $\delta\ra 0$.\par
Notice that in order to compute the gradient $\nabla f_\theta$ at the point 
$\theta$ the SPSA algorithm requires only $2$ measurements namely $f(\theta)$ and 
$f(\theta+\delta\Delta)$. 
An extremely useful consequence is that the gradient estimate is not affected by 
the number of dimensions. 

% As a result, the SPSA algorithm has been widley used 
% in a variety of domain where high dimensional parameters have to be tuned and is 
% also an appealing choice to tune the Hadoop parameters.\par
\begin{algorithm}
\caption{Simultaneous Perturbation Stochastic Approximation}
\label{algo}
\begin{algorithmic}[1]
\STATE Let initial parameter setting be $\theta_0\in X\subset\R^n$
\FOR{$n=1,2\ldots,N$}
\STATE Observe the performance of system $f(\theta_n)$.
\STATE Generate a random perturbation vector $\Delta_n \in\R^n$.
\STATE Observe the performance of system $f(\theta_n+\delta\Delta_n)$.
\STATE Compute the gradient estimate $\hat{\nabla} 
f_n(i)=\frac{f(\theta_n+\delta 
\Delta_n)-f(\theta_n)}{\delta\Delta_n(i)}.$\label{gres}
\STATE Update the parameter in the negative gradient direction 
$\theta_{n+1}(i)=\Gamma\big(\theta_n(i)-\alpha_n\frac{
f(\theta_n+\delta\Delta_n)-f(\theta_n)}{\delta\Delta_{n}(i)}\big)$.\label{update}
\ENDFOR
\RETURN $\theta_{N+1}$
\end{algorithmic}
\label{politer}
\end{algorithm}
The complete SPSA algorithm is shown in Algorithm~\ref{algo}, where
$\{\alpha_n\}$ is the step-size schedule and $\Gamma$ 
is a projection operator that keeps the iterates within $X$. We now briefly discuss 
the conditions and nature of the convergence of the SPSA algorithm. 
\input{schspsa} 

\subsection{Convergence Analysis}
The SPSA algorithm (Algorithm~\ref{algo}) makes use of a noisy gradient estimate 
(in line~\ref{gres}) and at each iteration takes a step in the negative gradient 
direction so as to minimize the cost function. The noisy gradient update can be 
re-written as 
\begin{align}\label{stocrec}
\theta_{n+1}&= \Gamma \left(\theta_n-\alpha_n\big(\mathbf{E}[\hat{\nabla}f_n | \theta_n] +\hat{\nabla}f_n -\mathbf{E}[\hat{\nabla}f_n | \theta_n ])\right)\\
	    &= \Gamma \left(\theta_n-\alpha_n\big(\nabla f_n + M_{n+1}+\epsilon_n )\right)\nn
\end{align}
where $M_{n+1}=\hat{\nabla}f_n -\mathbf{E}[\hat{\nabla}f_n | \theta_n]$ is an associated martingale 
difference sequence under the sequence of $\sigma$-fields 
$\mathfrak{F}_n = \sigma (\theta_m, m \leq n, \Delta_m, m < n), \; n \geq 1$ 
and $\epsilon_n$ is a small bias due to the $o(\delta)$ term 
in \eqref{gradestexp}.

The iterative update in \eqref{stocrec} is known as a stochastic approximation 
\cite{SA} recursion. As per the theory of stochastic approximation, in order to 
\emph{filter} out the noise, the step-size schedule $\{\alpha_n\}$ needs to 
satisfy the conditions below.
\begin{align}\label{ss}
\sum_{n=0}^\infty \alpha_n =\infty, \sum_{n=0}^\infty \alpha_n^2<\infty.
\end{align}
We now state the convergence result.
\begin{theorem}\label{conv}
As $n\ra \infty$ and $\delta\ra 0$, the iterates in \eqref{stocrec} (i.e., 
line~\ref{update} of Algorithm~\ref{algo}) converge to a set $A=\{\theta|\Gamma (\nabla 
f(\theta) ) =0\}$, where for any continuous 
$J: \R^n \ra \R^n$, $ \hat{\Gamma}(J(x)) = \lim\limits_{\eta \downarrow 0} \left( \frac{\Gamma (x + \eta J(x))- \Gamma (x)}{\eta} \right)$ .
\end{theorem}
\begin{proof}
 The proof is similar to Theorem 3.3.1, pp. 191-196 of \cite{kushner}.
\end{proof}


Theorem~\ref{conv} guarantees the convergence of the iterates to local minima. 
However, in practice local minima corresponding to small valleys are avoided due 
either to the noise inherent to the update or one can periodically inject some 
noise so as to let the algorithm explore further. Also, though the result stated in 
Theorem~\ref{conv} is only asymptotic in nature, in most practical cases 
convergence is observed in a finite number of steps. In the following section, we 
adapt SPSA to the problem of parameter tuning for enhancing the performance of Hadoop.
