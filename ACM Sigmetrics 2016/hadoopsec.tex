Hadoop is an open source implementation of the MapReduce\cite{ghemawat}, 
which has gained a huge amount of 
popularity in recent years as it can be used over commodity hardware. Hadoop 
 has two main components namely MapReduce and Hadoop Distributed File 
System(HDFS). The HDFS is used for storing data and MapReduce is used for 
performing computations over the data. 
We first discuss the HDFS and then MapReduce. Following this, we describe
the data flow analysis in Hadoop with an aim
to illustrate the importance of the various parameters.


\subsection{Hadoop Distributed File System}
Hadoop uses HDFS to store input and 
output data for the MapReduce applications. HDFS provides interfaces for 
applications to move themselves closer \cite{url_6} to where the data is located 
because data movement will be costly as compared to movement of small MapReduce 
code. It is fault tolerant and is optimized for storing large data sets. 

% A HDFS cluster (see Fig.~\ref{fig:hdfs}) consists of a single NameNode, a
A HDFS cluster (see \cite{hadoop}) consists of a single NameNode, a 
master server, and multiple slave DataNodes. 
The DataNodes, usually one per node, store the actual data 
used for computation. These manage the storage attached to the nodes 
that they run on. 
% They perform block creation, deletion, and replication upon 
% instruction from the NameNode. 
Internally, a file is split into one or more data 
blocks (block size is controlled by \emph{dfs.block.size}) and these blocks 
are stored in a set of DataNodes. They are responsible for serving read and 
write requests from the file system's clients.
NameNode manages the file system namespace and regulates access to 
files by clients. It has the following functions:
\begin{itemize}
 \item Store HDFS metadata and execute file systems operations on HDFS
%  \item Executes file system operations on files and directories stored on HDFS
 \item Mapping data blocks to DataNodes
 \item Periodically monitor the performance of DataNodes
\end{itemize}

\subsection{MapReduce}
A client application submits a MapReduce job. It is then split into various map 
and reduce tasks that are to be executed in the various cluster nodes. 
In MapReduce version 1 (v1), the JobTracker, usually running on a dedicated node, 
is responsible for execution and monitoring of jobs in the cluster.
It schedules map and reduce tasks to be run on the nodes in the cluster, 
which are monitored by a corresponding TaskTracker running on that particular node.
Each TaskTracker sends the progress of the corresponding map or reduce task to JobTracker
at regular intervals.
Hadoop MapReduce version 2 (v2, also known as Yet Another Resource Negotiator (YARN)\cite{yarn}) 
has a different architecture. 
It has a ResourceManager and NodeManager instead of JobTracker and TaskTracker. 
The tasks of resource and job management are distributed among resource manager and 
application master (a process spawned for every job) respectively.
The job submitted by a client application (for e.g., Terasort, WordCount benchmark applications) 
is associated with a NodeManager, which starts an ``Application Master'' in a container 
(a container is a Unix process, which runs on a node). The container architecture
utilizes cluster resources better, since YARN
manages a pool of resources that can be allocated based on need. This is unlike 
MapReduce v1 where each TaskTracker is configured with an inflexible map/reduce slot.
A map slot can only be used to run a map task and same with reduce slots.

\subsection{MapReduce Data Flow Analysis}
Map and Reduce are the two main phases of job processing (see Fig. \ref{hadooponearch}). 
The function of these phases is 
illustrated with the following simple example: 
\begin{example}
The objective is to count the number of times each word appears in a file whose 
content is given by,
\begin{center}
``This is an apple. That is an apple''.
\end{center}
The output of the Map operation is then given by, 
\begin{center}
	$<This,1> <is,1> <an,1><apple,1>$\\$<That,1><is,1><an,1><apple,1>$, 
\end{center}
following which the Reduce operation outputs
\begin{center}
    $<This, 1><That, 1> <is, 2> <an, 2> <apple, 2>$. 
\end{center}
Thus we obtain the count for each of the words. 
\end{example}
Map and Reduce phases can perform complex computations in contrast to the above example.
The efficiency of these computations and phases is controlled by various system parameters.
We describe the parameters our algorithm tunes (see Section \ref{exp}) and show how 
they influence the map and reduce phases of computation.

\begin{figure*}
\centering
\includegraphics[scale=0.22]{hadooponearch.png}
  \caption{MapReduce working}
  \label{hadooponearch}
\end{figure*}

\subsubsection{Map Phase}
Input data is split according to the input format (text, zip etc.) and split
size is controlled by the parameter \emph{dfs.block.size}.
For each split, a corresponding mapper task is created.

The \emph{Map} function retrieves data (records) from the input split 
with the help of the \emph{record reader}. The record reader provides 
the key-value pair (record) to the mapper according to the input format. 
The Map outputs $<key, value>$ and the meta-data (corresponding partition number of the 
key) according to the logic written in the map function. This output is 
written to a circular buffer, whose size is controlled by parameter 
\emph{mapreduce.task.io.sort.mb}. 
When the data in the buffer reaches a 
threshold, defined by \emph{mapreduce.map.sort.spill.percent}, 
data is \emph{spilled} to the local disk of a mapper node. 
The Map outputs will continue to be 
written to this buffer while the spill takes place. If any time the buffer 
becomes full, the Map task is blocked till spill finishes.

\emph{Sorting} (default \emph{Quick Sort}) and \emph{combine} operations are 
performed in the memory buffer prior to the data spill onto the disk. So 
increasing the buffer size of mapper decreases I/O cost but sorting cost will 
increase. \emph{Combine} executes on a subset of $<key,value>$ pairs. 
Combine is used for reducing data written to the disk. 
% Each spill file has an index information 
% about partition within file (like partition start \& end location). 
% If the index file (contains index information) size exceeds 
% \emph{mapreduce.task.index.cache.limit.bytes}, then the index data (metadata) 
% will be written to the disk with the corresponding spill file.


The \emph{merge} phase starts once the Map and the Spill phases 
complete. In this phase, all the spilled files from a mapper are merged together 
to form a single output file. Number of streams to be merged is controlled by 
the parameter \emph{mapreduce.task.io.sort.factor} (a higher value means more 
number of open file handles and a lower value implies  multiple merging rounds 
in the merge phase). After merging, there could be multiple records with same 
key in the merged file, so combiner could be used again. 
% At 
% this stage, the behavior of the combiner is controlled by 
% \emph{mapreduce.map.combine.minspills}. Mapper's final output could be 
% compressed by setting \emph{mapreduce.map.output.compress} property (setting 
% it to be \emph{true} lessens the network traffic and increases the CPU usage).

\subsubsection{Reduce Phase}
\emph{Reducers} are executed in parallel to the mappers if the 
fraction of map task completed is more than the value of 
\emph{mapreduce.job.reduce.slowstart.completedmaps} parameter, ot\\herwise  
reducers execute after mappers. The number of reducers for a work is controlled 
by \emph{mapreduce.job.reducers}. A Reducer fetches its input partition from 
various mappers via HTTP or HTTPS. The total amount of memory allocated to a 
reducer is set by \emph{mapreduce.reduce.memory.totalbytes} and the fraction 
of memory allocated for storing data fetched from mappers is set by 
\emph{mapreduce.reduce.shuffle.input.buffer.}\\\emph{percent}.
In order to create a single sorted data based on the key, the merge phase is
executed in order to collate the keys obtained from different partitions.
The number of map outputs needed to start this merge process is determined by 
\emph{mapreduce.reduce.merge.inmem.threshold}. The threshold for spilling the merged
map outputs to disk is controlled by
\emph{mapreduce.reduce.shuffle.merge.percent}.
Subsequent to all merge operations, reduce code is executed and its output is saved to the HDFS 
(as shown in Figure \ref{hadooponearch}). 

\subsubsection{Cross-Parameter Interaction}
The parameters \emph{io.sort.mb},\emph{reduce.input.buffer.percent} and 
\emph{shuffle.input.buffer.percent} control the number of spills written to disk.
Increasing the memory allocated will reduce the number of spill records in both Map and Reduce phases.
When \emph{io.sort.mb} is high, the spill percentage of Map (controlled by \emph{sort.spill.percent})
should be set to a high value. In the Reduce phase, the map outputs are merged and spilled to disk
when either the \emph{merge.inmem.threshold} or \emph{shuffle.merg\\e.percent} is reached.
Similarly, the \emph{task.io.sort.factor} determines the 
minimum number of streams to be merged at once, during sorting. 
So, on the reducer side, if there are say 40 mapper outputs and this value is set to 10, 
then there will be 5 rounds of merging (on an average 10 files for merge round).

The above examples indicate that changing one function in the reduce/map phase, affects 
other characteristics of the system. This implies that Hadoop system parameters cannot be tuned in isolation.
Each parameter has to be tuned by taking note of the values of related parameters.
Our SPSA-based method takes into account such relations between the parameters and 
appropriately tunes them to achieve enhanced performance.
In the next section, we discuss the existing work in the literature
which suggest techniques to enhance the performance of Hadoop.
