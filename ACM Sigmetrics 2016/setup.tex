The SPSA algorithm was presented in its general form in Algorithm~\ref{algo}. We 
now discuss the specific details involved in suitably applying the SPSA 
algorithm to the problem of parameter tuning in Hadoop.

\subsection{Mapping the Parameters}
The SPSA algorithm needs each of the parameter components to be real-valued i.e., in 
Algorithm~\ref{algo}, $\theta\in X \subset\R^n$. However, most of the Hadoop 
parameters that are of interest are not $\R^n$-valued. Thus, on the one hand we 
need a set of $\R^n$-valued parameters that the SPSA algorithm can tune and a 
mapping that takes these $\R^n$-valued parameters to the Hadoop parameters. In 
order to make things clear we introduce the following notation:

\begin{enumerate}
\item The Hadoop parameters are denoted by $\thetah$ and the $\R^n$-valued
parameters tuned by SPSA are denoted by $\thetaa$\footnote{Here subscripts $A$ 
and $H$ are abbreviations of the keywords Algorithm and Hadoop respectively}.
\item $S_i$ denotes the set of values that the $i^{th}$ Hadoop parameter can 
assume. $\thetahn(i)$, $\thetahx(i)$ and $\thetahd(i)$ denote the 
\emph{minimum}, \emph{maximum} and \emph{default} values that the $i^{th}$ 
Hadoop parameter can assume.
\item $\thetaa \in X\subset\R^n$ and $\thetah \in S_1\times\ldots \times S_n$. 
\item $\thetah=\mu(\thetaa)$, where $\mu$ is the function that maps $\thetaa\in 
X\subset \R^n$ to $\thetah \in S_1\times\ldots \times S_n$. 
\end{enumerate}

In this paper, we choose $X=[0,1]^n$, and $\mu$ is defined as 
$\mu(\thetaa)(i)=\lfloor(\thetahx(i)-\thetahn(i))\thetaa(i)+\thetahn(i)\rfloor$ 
and 
$\mu(\thetaa)(i)=(\thetahx(i)-\thetahn(i))\thetaa(i)+\thetahn(i)$ for integer 
and real-valued Hadoop parameters respectively.

\subsection{Perturbation Sequences and Step-Sizes}
We chose $\delta\Delta_n \in \R^n$ be independent random variables, such that 
$Pr\{\delta\Delta_n(i)=-\frac{1}{\thetahx(i)-\thetahn(i)}\} 
=Pr\{\delta\Delta_n(i)=+\frac{1}{\thetahx(i)-\thetahn(i)}\}=\frac{1}{2}$.
This perturbation sequence ensures that the Hadoop parameters assuming only 
integer values change by a magnitude of at least $1$ in every perturbation. 
Otherwise, using a perturbation whose magnitude is less than 
$\frac{1}{\thetahx(i)-\thetahn(i)}$ might not cause any change to the 
corresponding Hadoop parameter resulting in an incorrect gradient estimate.

% \subsection{Choice of Step-Sizes}
The conditions for the step-sizes in \eqref{ss} are asymptotic in nature and are 
required to hold in order to be able to arrive at the result in 
Theorem~\ref{conv}. However, in practice, a constant step size can be used since 
one reaches closer to the desired value in a finite number of iterations. 
%In such cases, the step-sizes are to be chosen by useful rules of thumb. 
We know apriori that the parameters tuned by the SPSA algorithm belong to the 
interval $[0,1]$ and it is enough to have step-sizes of the order of 
$\min_i(\frac{1}{\thetahx(i)-\thetahn(i)})$ (since any finer step-size used to 
update the SPSA parameter $\thetaa(i)$ will not cause a change in the 
corresponding Hadoop parameter $\thetah(i)$). In our experiments, we chose 
$\alpha_n=0.01, \forall n\geq 0$ and observed convergence in about $20$ 
iterations.
