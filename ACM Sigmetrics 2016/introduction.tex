We are in the era of big data and huge volumes of data are generated in various 
domains like social media, financial markets, transportation and health care. 
Faster analysis of such big unstructured data is a key requirement for achieving 
success in these domains. Popular instances of such cases include distributed 
pattern-based searching, distributed sorting, web link-graph reversal, singular 
value decomposition, web access log stats, inverted index construction and 
document clustering. Extracting  hidden patterns, unknown correlations and other 
useful information is critical for making better decisions. 
Many industrial organisations like Yahoo!, Facebook, Amazon etc. need to handle
and process large volumes of data and their product success hinges on this ability.
Thus, there is a 
need for parallel and distributed processing/programming methodologies that can 
handle big data using resources built out of commodity hardware. Currently available 
parallel processing systems are database systems \cite{pavlo} like Teradata, Aster Data, Vertica etc.,
which are quite robust and are high-performance computing platforms. 
However, there is a need for a parallel processing system which
can handle large volumes of data using low-end servers and which is easy-to-use.
\textbf{MapReduce}\cite{ghemawat} is one-such programming model.

MapReduce computation over input data goes through two phases namely \emph{map} 
and \emph{reduce}. At the start of the map phase, the job submitted by the client is 
split into multiple map-reduce tasks that are to be executed by  
various worker nodes. The map phase then 
creates the key-value pairs from the input dataset according to the user defined 
map. The reduce phase makes use of the key-value pairs and aggregates according 
to user specified function to produce the output.

Apache \textbf{Hadoop}\cite{hadoop} is an open-source implementation of MapReduce written in 
Java for distributed storage and processing of very large data sets on clusters 
built using commodity hardware.
The Hadoop framework gives various parameter (knobs) that need to be tuned 
according to the program, input data and hardware resources. It is important to 
tune these parameters to obtain best performance for a given MapReduce job.
The problem of Hadoop performance being limited by the parameter configuration
was recognized in \cite{kambatla}.
Unlike SQL, MapReduce jobs cannot be modeled using a small and finite 
space of relational operators \cite{pavlo}. Thus, 
it is not straight forward to quantify the effect of these various parameters on 
the performance and hence it is difficult to compute the best parameter 
configuration apriori. In addition, difficulty in tuning these parameters also 
arises due to two other important reasons. Firstly, due to the presence of a 
large number of parameters (about $200$, encompassing a variety of functionalities) 
the search space is large and 
complex.
Secondly, there is a pronounced effect of cross-parameter interactions, i.e., 
the knobs are not independent of each other. For instance, increasing the 
parameter corresponding to map-buffer size will decrease the I/O cost, however, 
the overall job performance may degrade because sorting cost may increase (in 
quick sort, sorting cost is proportional to the size of data). The complex 
search space along with the cross-parameter interaction does not make Hadoop 
amenable to manual tuning.


The necessity for tuning of Hadoop parameters was first emphasized in 
\cite{kambatla}, which proposed a method to determine the optimum
configuration given a set of computing resources.
Recent efforts in the direction of automatic tuning of the Hadoop parameters include 
Starfish\cite{starfish}, AROMA\cite{aroma},
MROnline\cite{mronline}, 
PPABS \cite{ppabs} and JellyFish \cite{jellyfish}. 
We observe that collecting statistical data to create virtual profiles and 
estimating execution time using mathematical  model (as in 
\cite{kambatla,starfish,jellyfish,aroma,mronline,ppabs}) requires significant level of expertise which 
might not be available always. In addition, since Hadoop MapReduce is evolving 
continuously with a number of interacting parts, the mathematical model also has 
to be updated and in the worst case well-defined mathematical model might not be 
available for some of its parts due to which a model-based approach might fail. 
Further, given the presence of cross-parameter interaction it is a good idea to 
retain as many parameters as possible (as opposed to reducing the parameters 
\cite{ppabs}) in the tuning phase.

In this paper, we present a novel tuning methodology based on a noisy gradient 
method known as the simultaneous perturbation stochastic approximation (SPSA) 
algorithm \cite{spall}. The SPSA algorithm is a black-box stochastic optimization 
technique which has been applied to tune parameters in a variety of complex 
systems. 
An important feature of the SPSA is that it 
utilizes observations from the real system as feedback to tune the 
parameters. Also, the SPSA algorithm is dimensionality free, i.e., it needs only 
$2$ or fewer observations per iteration irrespective of the number of parameters 
involved. In this paper, we adapt the SPSA algorithm to tune the 
parameters used by Hadoop to allocate resources for program execution.

\subsection{Our Contribution}
Our aim is to introduce the practitioners to a new method that is different 
in flavour from the prior methods, simple to implement and effective at the same 
time. The highlights of our SPSA based approach are as follows:
\begin{itemize}
\item \textbf{Mathematical model}: The methodology we propose utilizes the 
observations from the Hadoop system and does not need a mathematical model. This is desirable since 
mathematical models developed for older versions might not carry over to newer 
versions of Hadoop.
\item \textbf{Dimension free nature}: SPSA is designed to handle complex 
search spaces. Thus, unlike \cite{Babu10towardsautomatic} reducing the search 
space is not a requirement.
\item \textbf{Parametric dependencies}: Unlike a host of black-box optimization 
methods that depend on clever heuristics, our SPSA based method computes 
the \emph{gradient} and hence takes 
into account the cross parameter interactions in the underlying problem.
\item \textbf{Performance}: Using the SPSA algorithm we tune $11$ parameters 
simultaneously. 
Our method provides a 66\% decrease 
in execution time of Hadoop jobs on an average, when compared to the default configuration. 
Further, we also observe a reduction of 45\% in execution times, 
when compared to prior \cite{starfish} methods.
% 
% We observe a reduction of  $35$, $27$, $55$ percent in the case 
% of a $4$-node cluster and  $52$, $19$, $71$ percent in the case of a $6$-node 
% cluster for benchmark Hadoop applications namely, \emph{Word co-occurrence}, 
% \emph{Grep} and \emph{Terasort} respectively.
\end{itemize}

\subsection{Organisation of the Paper}
\begin{comment}
In the next section, we present the noisy gradient approach to the problem 
of automatic parameter tuning. Following it, in Section~\ref{relatedwork} we discuss the related work 
and contrast it with our approach. We provide a detailed description of our 
SPSA-based approach in Section \ref{spsa}. A brief description of Hadoop data flow analysis is 
presented in Section \ref{hadoop}. This section also  
illustrates the importance of the parameters we tune in this paper. In 
Section \ref{setup} we discuss the specific details in implementing the SPSA 
algorithm to tune the Hadoop parameters. We describe the experimental setup and 
present the results in Section \ref{exp}. Section \ref{concl} concludes the paper and suggests 
future enhancements.
\end{comment}
In the next section, we describe the Hadoop architecture, its data flow analysis and 
point out the importance and role of some of the configuration parameters.
Following it, in Section~\ref{relatedwork} we discuss the related work 
and contrast it with our approach. We provide a detailed description of our 
SPSA-based approach in Section \ref{autotune}.
In Section \ref{setup} we discuss the specific details in implementing the SPSA 
algorithm to tune the Hadoop parameters.
We describe the experimental setup and present the results in Section \ref{exp}. 
Section \ref{concl} concludes the paper and suggests future enhancements.