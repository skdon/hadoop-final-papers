The performance of various complex systems such as  traffic control, 
unmanned aerial vehicle (UAV) control etc.
depends on a set of tunable parameters (denoted by $\theta$). 
Parameter tuning in such cases is difficult because of the \emph{black-box} nature of the problem and the 
\emph{curse-of-dimensionality}. In this section, we 
discuss the general theme behind the methods that tackle these bottlenecks and 
their relevance to the problem of tuning the Hadoop parameters.
\subsection{Bottlenecks in Parameter Tuning}
\input{bbopti}
In many systems, 
% the exact nature of the dependence of the performance 
% on the parameters is not known explicitly i.e., 
the performance of the system cannot be 
expressed as an analytical function of the parameters. As a result, the 
parameter setting that offers the best performance cannot be computed apriori. 
However, the performance of the system can be observed for any given parameter 
setting either from the system or a simulator of the system. In such a scenario, 
one can resort to \emph{simulation-based} or \emph{black-box} optimization methods 
that tune the parameters based on the output observed from the system/simulator 
without knowing its internal functioning. 
As illustrated in Fig. \ref{bbopti}, the simulation 
optimization scheme sets the current value of the parameter based on the past 
observations. 
% The way in which past observation is used to compute the current 
% parameter setting varies across methods.

An important issue in the context of simulation optimization is the number of 
observations and the cost of obtaining an observation from the system/simulator. 
The term \emph{curse-of-dimensionality} denotes the exponential increase in the 
size of the search space as the number of dimensions increases. 
In addition, in many applications, the parameter $\theta \in X$, where $X \subset \R^n$. Since it is computationally 
expensive to search such a large parameter space, it is important for 
simulation optimization methods to make as few observations as possible.
Hadoop MapReduce  exhibits black-box nature because it is not well 
structured like SQL.
In addition, cross-parameter interactions also affect the performance, and hence 
it is not possible to treat the parameters independent of each other. Besides, the 
problem is also afflicted by the curse-of-dimensionality.

\subsection{Noisy Gradient based Optimization}
In order to take the cross-parameter interactions into account, one has to make 
use of the sensitivity of the performance measure with respect to each of the 
parameters at a given parameter setting. This sensitivity is formally known as 
the \emph{gradient} of the performance measure at a given setting. If there are $n$ parameters to tune,
then it takes only $O(n)$ observations to compute the gradient 
of a function at a given point. However, even $O(n)$ computations are not 
desirable if each observation is itself costly.

Consider the noisy gradient scheme given in \eqref{ngrad} 
below.
\begin{align}\label{ngrad}
\theta_{n+1}&=\theta_n-\alpha_n\big(\nabla f_n +M_n\big),
\end{align}
where $n=1,2\ldots$ denotes the iteration number, $\nabla f_n\in \R^n$ is the 
gradient of function $f$, $M_n\in \R^n$ is a zero-mean noise sequence and $\alpha_n$ is the 
step-size. 
\input{noisegrad}
Fig. \ref{noisegrad} presents an intuitive picture of how a noisy gradient algorithm works. 
Here, the algorithm starts at $\theta_0$ and needs to move to $\theta^*$ which is the 
desired solution. The green lines denote the true gradient step (i.e., $\alpha_n \nabla f_n$) 
and the dotted circles show the region of uncertainty due to the noise term $\alpha_n M_n$. 
The red line denotes the fact that the true gradient is disturbed and each iterate is 
\emph{pushed} to a different point within the region of uncertainty. The idea 
here is to use diminishing step-sizes to filter the noise and eventually move 
towards $\theta^*$. 
The simultaneous perturbation stochastic approximation (SPSA) 
algorithm is a \emph{noisy gradient} algorithm which works as illustrated in Fig. \ref{noisegrad}.
It requires only $2$ observations per iteration. 
Thus the SPSA algorithm is extremely useful in cases 
when the dimensionality is high and the observations are costly.
We adapt it to tune the parameters of Hadoop. By adaptively tuning the Hadoop parameters,
we intend to optimize the Hadoop job execution time, which is the performance metric (i.e., $f(\theta)$) in 
our experiments. 
% Note that we can also have other performance metrics - like number of records spilled to disk,
% memory and heap usage or number of failed jobs. 
% Next, we provide a detailed description of SPSA.

\subsection{Simultaneous Perturbation Stochastic Approximation (SPSA)}
\label{spsa}
\input{spsa}