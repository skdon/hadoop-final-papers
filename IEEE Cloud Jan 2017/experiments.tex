We use Hadoop versions 1.0.3 and 2.7.3 in our experiments. The SPSA
algorithm described in Algorithm \ref{politer} is implemented
as a process which executes in the Resource Manager (and/or NameNode).
First, we justify the selection of parameters to be tuned in our experiments. Then, 
we give details about the implementation.

\subsection{Parameter Selection}
Based on the data flow analysis of Hadoop MapReduce (see \cite{hadoop}) we identify 11 parameters which are found to critically
affect the operation of HDFS and the Map/Reduce operations. These are listed in Table \ref{tab:opt_parameter}.
% Parameters chosen from Hadoop v1 differ in some cases
% from that of Hadoop v2, as Hadoop v2 no longer supports
% few parameters of the older version (for e.g., \emph{io.sort.record.percent}).
% Additionally, there is a major difference in how the jobs are handled in these two versions. 
% In Hadoop v1, there are fixed map and
% reduce slots, but in Hadoop v2 jobs are executed in containers 
% (see \cite{hadoop}).
Numerous parameters of Hadoop deal with book keeping related tasks, whereas some other parameters
are responsible for the performance of underlying operating system tasks.
For e.g., \textit{mapred.child.java.opts} is a parameter related to the Java virtual machine (JVM) of 
Hadoop. We avoid tuning such parameters, which are best left for low-level OS optimization.
Instead, we tune parameters which are directly Hadoop dependent, for e.g., number of reducers, I/O utilization
parameter etc. 
% However, even with $11$ parameters, the search space is still large and complex. 
% To see this, if each parameter can assume say $10$ different values then the search space 
% contains $10^{11}$ possible parameter settings. 
% Some parameters have been left out because 
% either they are  backward incompatible or they incur additional overhead in implementation. 

\subsection{Cluster Setup}
Our Hadoop cluster consists of $25$ nodes. Each node has a 8 core Intel Xeon E3, 2.50 GHz 
processor, $3.5$ TB HDD, $16$ GB memory, 1 MB L2 Cache, 8MB L3 Cache
and connected through a Gigabit network.
One node works as the 
Master node and the rest of the nodes are used as slave nodes (DataNodes).
% For optimization and evaluation purpose we set the number of map slots to $3$ and 
% reduce slots to $2$ per node (only in Hadoop v1). Hence, in a single wave of Map jobs processing, the cluster can process 
% $24 \times 3 = 72$ map tasks  and $24 \times 2 = 48$ reduce tasks (for more details see \cite{hadoop}). 
% HDFS block replication was set to $2$. 
We use a dedicated Hadoop cluster in our experiments, which
is not shared with any other application.

\subsection{Benchmark Applications}
In order to evaluate the performance of tuning algorithm, we use representative applications.
These applications are listed in Table \ref{tab:opt_parameter}.
Terasort application takes as input a text data file and sorts it. It has two main components - 
\emph{TeraGen} - which generates the input data for sorting algorithm and \emph{TeraSort} - algorithm that 
implements sorting.
The Grep application searches for a particular pattern in a given input file.
Bigram application counts all unique sets of two consecutive words in a set of documents, 
while the Inverted index application generates word to document indexing from a list of documents. 
Word Co-occurrence is a popular Natural Language Processing program which
computes the word co-occurrence matrix of a large text collection.
As can be inferred, Grep and Bigram applications are CPU intensive, while
the Inverted Index and TeraSort applications are both CPU and memory intensive.
% These benchmark applications can be further categorized as map-intensive, reduce-intensive etc.
\subsection{Learning Phase}
SPSA runs a Hadoop job with a different configuration in each iteration. 
We refer to these iterations as the \emph{optimization} or the \emph{learning} phase.
The algorithm eventually converges to an optimal value of the configuration parameters. 
The job execution time corresponding to the converged parameter vector is optimal 
for the corresponding application.
During our evaluations we have seen that SPSA 
converges within $10$ - $15$ iterations and within each iteration 
it makes two observations, i.e. it executes Hadoop job twice taking the total count of 
Hadoop runs during the optimization phase to $20$ - $30$. It is of utmost importance 
that the optimization phase is fast otherwise it can overshadow the benefits which 
it provides.

In order to ensure that the optimization phase is fast, we execute the Hadoop jobs 
on a subset of the workload. 
Deciding the size of this ``partial'' workload is crucial
as the run time on a small work load is eclipsed by the job setup and cleanup 
time. We take cue from Hadoop processing system in order to determine the size of the 
partial workload.
Hadoop splits the input data based on the 
block size of HDFS and spawns a map for each of the splits.
The number of map tasks that can run paralley at a given time is 
upper bounded by the total map slots available in the cluster. Using this fact, the 
size of the partial data set which we use is equal to twice the number of 
map slots in the cluster multiplied by the data block size. Hadoop finishes the 
map task in two waves of the maps jobs which allows the SPSA to capture the 
statistics with a single wave and the cross relations between two successive waves.
Our expectations (and borne by our results) is that the value of configuration parameters which optimize these two 
waves of Hadoop job also optimize all the subsequent waves as those are repetitions of 
similar map jobs.

\subsection{Experimental Setup}
For evaluating the performance of SPSA on different benchmarks, two waves of map tasks during job execution were ensured.
Further, we selected workloads such that the execution time with default configuration is at least $10$ minutes.
This was done to avoid the scenario where the job setup and cleanup time overshadows 
the actual running time and there is practically nothing for SPSA to optimize.

In the cases of Bigram and Inverted Index benchmark executions, we observed that even with small 
amount of data, the job completion time is high (since they are reduce-intensive operations).
So, due to this reason, we used small sized input data files. Using small sized input data files
resulted in the absence of two waves of map tasks. However, since in these applications,
reduce operations take precedence, the absence of two waves of map tasks did not create
much of a hurdle.

We optimize Terasort using a partial data set of size $30$GB, Grep on $22$GB,
Word Co-occurrence on $1$GB, Inverted Index on $1$GB and Bigram count on $200$MB
of data set. In learning phase, for each benchmark, we use the default configuration as the initial point
for the optimization. 
% Table \ref{tab:opt_parameter} indicates
% the default values and the values provided by SPSA for the parameters we tune.
For Word-Cooccurrence, Grep and Bigram benchmarks we have used Wikipedia dataset\cite{mronline}($\approx50$GB).

The SPSA algorithm terminates when either the change in gradient estimate is negligible or 
the maximum number of iterations have been reached. An important point to note is that Hadoop parameters can take
values only in a fixed range. We take care of this by projecting the tuned parameter values 
into the range set (component-wise).
% A penalty-function can also be used instead. 
% If noise level in the function evaluation is high then, 
%  it is useful to average several SP gradient estimates (each with 
% independent values of $\Delta_{k}$) at a given  $\theta_{k}$.  Theoretical 
% justification for net improvements to efficiency by such gradient averaging is 
% given in ~\cite{Spall92multivariatestochastic}. We can also use a one evaluation variant of SPSA,
% which can reduce the per iteration cost of SPSA. 
% But it has been shown \cite{Spall92multivariatestochastic} that standard two function measurement form, 
% which we use in our work is more efficient (in terms of total number of loss 
% function measurements) to obtain a given level of accuracy in the $\theta$ 
% iterate. 
% \subsection{Experimental Setup for Performance Comparison}

We compare our method with Starfish\cite{starfish} and Profiling and Performance Analysis-based System 
(PPABS) \cite{ppabs} frameworks.
Starfish is designed for Hadoop version 1
only, whereas PPABS works with the recent versions also. Hence, in our experiments we use both versions
of Hadoop. To run Starfish, we use the executable hosted by the authors of \cite{starfish} to profile
the jobs run on partial workloads. Then execution time of new jobs is obtained by running the jobs 
using parameters provided by Starfish. For testing PPABS, we collect and use the datasets as described in \cite{ppabs}.
% cluster them and find optimized parameters (using simulated annealing) for each cluster.
% Each new job is then assigned to one cluster and executed with the parameters optimized for that cluster.

\subsection{Discussion of Results}
\input{opt_parameter}
\input{benchConverge}
% \input{benchConverge2}
\input{benchPerfComparev1-brief}
\input{benchPerfComparev2}
Our method starts optimizing with the default configuration,
hence the first entry in Fig. \ref{fig:spsaConv} show the execution time
of Hadoop jobs for the default parameter setting. 
It is important to note that the jumps in the plots are due to the
noisy nature of the gradient estimate and they eventually
die down after sufficiently large number of iterations.
As can be observed from Fig. \ref{fig:spsaConv}, SPSA reduces the 
execution time of Terasort benchmark by $60-63\%$ when compared to default settings
and by $40-60\%$ when compared to Starfish optimizer.
For Inverted Index benchmark the reduction is $~80\%$ when compared to default settings.
In the case of word co-occurrence, the observed reduction is $22\%$ on average when compared to default settings
and $2\%$ on average when compared to Starfish. For large input sizes, the reduction in execution times of 
Word Co-occurrence jobs is high when compared to jobs with small input sizes.

SPSA finds the optimal configuration while keeping the relation among the parameters in mind. 
For Terasort, a small value $(0.14)$ of \emph{io.sort.spill.percent} will generate a lot of spilled 
files of small size. Because of this, the value of \emph{io.sort.factor} has been increased to $475$ 
from the default value of $10$. This ensures the combination of number of spilled files 
to generate the partitioned and sorted files.
A small value of \emph{shuffle.input.buffer.percent} $(0.14)$ and a large value of 
\emph{inmem.merge.threshold} $(9513)$ may be confusing as both of them act as a threshold 
beyond which in-memory merge of files (output by map) is triggered.
The reason for this is that the size of files spilled during the process will be small 
because of low value of \emph{io.sort.spill.percent}, and hence filling
of $0.14\%$ of reduce memory will take a large number of files. 
Default value of number of reducers (i.e., $1$) generally does not work in practical situations. 
However, increasing it to a very high number also creates an issue as it results in more network and disk overhead.
SPSA optimizes this based on the map output size.
% As can be observed in Table \ref{tab:opt_parameter},
% \emph{mapred.compress.map.output} is set to true for Terasort benchmark.
% This is because, the output data of Map phase has same size as the input data (which might be huge).
% Thus, in such scenarios, it is beneficial if the Map output is compressed.
Grep benchmark, on the other hand produces very little map output, and even smaller sized data to be shuffled. 
Hence \emph{io.sort.mb} value is reduced to $50$ from default $100$ (see Table \ref{tab:opt_parameter})
and number of reducers is set to $1$. Further, value of 
\emph{inmem.merge.threshold} has been reduced to $681$ from $1000$ as there is not much data to work on.
% % Bigram and Inverted Index are computationally expensive operations.
% % Hence \emph{io.sort.mb} is increased to $751$ and $617$ respectively. 
% Both of these applications also generate a reasonable size of data during the map phase,
% which implies a lot of spilled files are generated. 
% Thus, \emph{inmem.merge.threshold} has been increased to $4201$ and $3542$ respectively.
Similar arguments can be made for the value of parameters tuned for Hadoop v2.
The difference in the values of the tuned parameter in Hadoop 1 and Hadoop 2 for the same benchmark 
arises because of inherent differences in their architecture and how jobs are executed.

\subsection{Advantages of SPSA}
\input{reltable}
The above discussion indicates that SPSA performs well in optimizing Hadoop
parameters. We highlight other advantages (also see Table \ref{tab:rel}) of using 
our proposed method:
\begin{enumerate}
\item Parameters can be easily added and removed from the set of tunable parameters, 
 which make our method suitable for scenarios where the user wants to have control over the parameters to be tuned.
 \item Independent of Hadoop version: Methods like \cite{starfish,mronline} 
 use the internal Hadoop structure to place ``markers'' for precisely profiling a job. 
 \cite{starfish} uses btrace to observe the time spent in each function. A small change in the source code makes \cite{starfish}
 unusable (\cite{starfish} supports only Hadoop versions $0.20.x$ and $1.0.3$).
 SPSA does not rely on the internal structure of Hadoop and only observes the final execution time (easily available).
%  \item \textbf{Independent of Hadoop version:} As mentioned previously, profiling-based methods
%  are highly dependent on the MapReduce version and any changes in the source code of Hadoop will require
%  a version upgrade of these methods. In contrast, our SPSA-based method does not rely on any specific 
%  Hadoop version.
%  
%  \item \textbf{Pause and resume:} SPSA optimizes the parameters iteratively. It starts at a given point 
%  (default setting in our case) 
%  and then progressively finds a better configuration (by estimating gradient). 
%  Such a process can be paused at any iteration and then resumed using the same parameter configuration, 
%  where the iteration was stopped.
%  This is unlike the profiling-based methods, which need to profile jobs in one go. 
%   
 \item SPSA takes into consideration multiple values of execution time of a job for the same parameter setting
 (randomness in execution time).
 This is not the case in other methods, which profile a job only once. 
 Multiple observations helps SPSA to remove the randomness in the job which arise due to the underlying hardware. 

 \item Profiling takes a long time (since job run time is not yet optimized 
 during profiling) which adds an extra overhead for these methods like Starfish, PPABS, MROnline etc. For e.g.,
 in our experiments, Starfish profiling executed for $4$ hours, $38$ minutes in the case of Word co-occurrence
 benchmark on Wikipedia data of size $4$ GB. 
 In contrast, our method does not incur additional ``profiling'' time.
\end{enumerate}