We are in the era of big data and huge volumes of data are generated in various 
domains like social media, financial markets, transportation etc. 
Quick analysis of such big and unstructured data is a key requirement for achieving 
success in many of these domains. 
% Popular instances of such analysis include distributed 
% pattern-based searching, distributed sorting, etc. 
Performing distributed sorting, extracting  hidden patterns and unknown correlations and other 
useful information is critical for making better decisions. 
Many organizations like Yahoo!, Facebook, Amazon etc. need to handle
and process large volumes of data as their product success hinges on this ability.
Thus, there is a 
need for parallel and distributed processing/programming methodologies that can 
handle big data using resources built out of commodity hardware. 

% State-of-the-art parallel processing systems are database systems \cite{pavlo} like Teradata, Aster Data, Vertica etc.,
% which are quite robust and are capable of high-performance computing. 
% However, there is a need for a parallel processing system which
% can handle large volumes of data using commodity hardware.
\textbf{MapReduce}\cite{ghemawat} is  a programming model
which uses resources built out of commodity hardware.
It is a computation framework which
is optimized to process large amounts of data.
MapReduce operation/logic is based on $<key, value>$ pairs. 
% There are two main phases of a MapReduce job, namely Map and Reduce. 
% MapReduce deviates from the norm of other computation
% framework as it takes code to the data (compared to other approaches, 
% where data is taken to the code).
It processes data quickly by running Map tasks in parallel on different nodes, 
manages all communications and data transfers between the various nodes, and provides for redundancy and fault tolerance.

Apache \textbf{Hadoop}\cite{hadoop} is an open-source implementation of MapReduce.
Besides MapReduce, it comprises of the Hadoop Distributed File 
System (HDFS), which stores data and manages the Hadoop cluster (see \cite{hadoop}).
Hadoop cluster consists of a single NameNode and multiple slave DataNodes. 
The NameNode acts as a master server, while the DataNodes store the actual data 
used for computation. 
The Hadoop framework provides different parameters that can be tuned 
according to the workload, input data and hardware resources.
For e.g., a file is split into one or more data 
blocks and these blocks are stored in a set of DataNodes. The block size is controlled by a 
parameter \emph{dfs.block.size}, which can be set based on the input data size of the workload
and the cluster configuration. 
The performance of Hadoop hinges on the values of such parameters.
The default values of these parameters generally do not give the optimal performance. Therefore,
it is important to tune these parameters according to the workload.
The best parameter configuration cannot be computed apriori, because
it is not straight forward to quantify the effect of the various parameters on 
the performance. Hence, it is difficult to compute the best parameter 
configuration apriori. In addition, difficulty in tuning these parameters also 
arises due to two other important reasons. First, due to the presence of a 
large number of parameters (about $200$, encompassing a variety of functionalities) 
the search space is large and complex.
Second, there is a pronounced effect of cross-parameter interactions, i.e., 
the knobs are not independent of each other. For instance, 
the parameter \emph{io.sort.mb} controls the number of spills written to disk (map phase). 
If it is set high, the spill percentage of Map (controlled by \emph{sort.spill.percent})
should also be set to a high value.
Thus, the complex search space along with the cross-parameter interaction does not make Hadoop 
amenable to manual tuning.


Some early works \cite{Ganapathi:EECS-2009-181,cmureport} have focussed on analysing 
the Hadoop performance but do not address the problem of parameter tuning.
The necessity for tuning of Hadoop parameters was first emphasized in 
\cite{kambatla}. 
Attempts toward building an optimizer for hadoop performance started with Starfish\cite{starfish}.
% In Starfish \cite{starfish,journals/pvldb/HerodotouB11}, a \emph{Profiler} collects detailed 
% statistical information (like data flow and cost statistics) from unmodified Mapreduce program 
% during full or partial execution. Then, a \emph{What-if} engine estimates the 
% cost of a new job without executing it on real system (uses mathematical model and collected statistics). 
% The \emph{cost-based optimizer} (CBO) uses the what-if 
% engine and recursive random search (RSS) for tuning the parameters for a new workload.
Recent efforts in the direction of automatic tuning of the Hadoop parameters include 
MROnline\cite{mronline} and
PPABS \cite{ppabs}. 
We observe that collecting statistical data to create virtual profiles and 
estimating execution time using mathematical  model (as in 
\cite{kambatla,starfish,mronline,ppabs}) requires significant level of expertise. 
In addition, since Hadoop MapReduce is evolving 
continuously with a number of interacting parts, the mathematical model also has 
to be updated and in the worst case well-defined mathematical model might not be 
available for some of its parts due to which a model-based approach might fail. 
Further, given the presence of cross-parameter interaction it is a good idea to 
retain as many parameters as possible (as opposed to reducing the parameters 
\cite{ppabs}) in the tuning phase. 

In order to address the above shortcomings, we suggest a method that directly utilizes the 
data from the real system and tunes the parameters via \emph{feedback} 
(see Fig. \ref{approaches}). This approach is based on a noisy gradient 
method known as the simultaneous perturbation stochastic approximation (SPSA) 
algorithm \cite{Spall92multivariatestochastic}. The SPSA algorithm is a black-box stochastic optimization 
technique which is independent of the number of parameters to tune.
In this paper, we adapt the SPSA algorithm to tune the 
parameters used by Hadoop to allocate resources for program execution.
% \subsection{Motivation for Our Approach}
% \input{motivation}
\input{approaches}
% 
% Thus, we are motivated to adapt SPSA algorithm 
% to tune the parameters. 
We believe that the SPSA based scheme is of interest to 
practitioners because it does not require any model building and it uses only the 
\emph{gradient} estimate at each step. Through the gradient estimate, it takes the cross 
parameter interaction into account. Further, the SPSA algorithm is not limited 
by the parameter dimension.
% \subsection{Our Contribution}

Our aim is to introduce the practitioners to a new method that is different 
in flavour from the prior methods, simple to implement and effective at the same 
time. The highlights of our SPSA based approach are as follows:
\begin{itemize}
\item \textbf{Mathematical model}: Our methodology utilizes the 
observations from the Hadoop system. This is desirable since 
mathematical models developed for older versions might not carry over to newer 
versions of Hadoop.
\item \textbf{Dimension free nature}: SPSA is designed to handle complex 
search spaces. 
\item \textbf{Parametric dependencies}: Unlike other black-box optimization 
methods that depend on clever heuristics, our SPSA based method computes 
the \emph{gradient} and hence takes into account the cross parameter interactions 
in the underlying problem.
\item \textbf{Performance}: Using the SPSA algorithm we tune $11$ parameters 
simultaneously (for both Hadoop versions). 
Our method provides a reduction of 45-66\% decrease 
in execution time of Hadoop jobs on an average, when compared to prior methods.
\end{itemize}

\subsection{Organisation of the Paper}
In the next section, 
we provide a detailed description of our 
SPSA-based approach in Section \ref{autotune} and provide 
specific details in adapting the SPSA 
algorithm to tune the Hadoop parameters.
We describe the experimental setup and present the results in Section \ref{exp}. 
Section \ref{concl} concludes the paper and suggests future enhancements.